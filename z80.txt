/*
 * Copyright (C) 2017 bzt (bztsrc@gitlab)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Instruction table for Zilog 80 architecture
 */

@regs	B C D E H L ? A
@reg16	BC DE HL SP IX IY
@conds	NZ Z NC C PO PE P M
<A>	"a"
<I>	"i"
<R>	"r"
<Cidx>	"(c)"
<r>	@regs[r]
<g>	"%s'", @regs[g]
<r16>	@reg16[d]
<c>	@conds[c]
<af>	"af"
<afap>	"af'"
<sp>	"sp"
<spidx>	"(sp)"
<n>	"%xh", n
<nd>	"%d", n
<nidx>	"(%xh)", n
<n8>	"%xh", n<<3
<hl>	"hl"
<hlidx>	"(hl)"
<bcidx>	"(bc)"
<de>	"de"
<deidx>	"(de)"
<ix>	"ix"
<ixidx>	"(ix)"
<ixd>	"(ix+%d)", d
<iy>	"iy"
<iyidx>	"(iy)"
<iyd>	"(iy+%d)", d
<labeln>	"%xh", n
<labeli>	"%lxh", $+i

01rrrggg	LD	<r>, <g>
nnnnnnnn00rrr110	LD	<r>, <n>
01rrr110	LD	<r>, <hlidx>
dddddddd01rrr11011011101	LD	<r>, <ixd>
dddddddd01rrr11011111101	LD	<r>, <iyd>
01110rrr	LD	<hlidx>, <r>
dddddddd01110rrr11011101	LD	<ixd>, <r>
dddddddd01110rrr11111101	LD	<iyd>, <r>
nnnnnnnn00110110	LD	<hlidx>, <n>
nnnnnnnndddddddd0011011011011101	LD	<ixd>, <n>
nnnnnnnndddddddd0011011011111101	LD	<iyd>, <n>
00001010	LD	<A>, <bcidx>
00011010	LD	<A>, <deidx>
nnnnnnnnnnnnnnnn00111010	LD	<A>, <nidx>
00000010	LD	<bcidx>, <A>
00010010	LD	<deidx>, <A>
nnnnnnnnnnnnnnnn00110010	LD	<nidx>, <A>
0101011111101101	LD	<A>, <I>
0101111111101101	LD	<A>, <R>
0100011111101101	LD	<I>, <A>
0100111111101101	LD	<R>, <A>
nnnnnnnnnnnnnnnn00dd0001	LD	<r16>, <n>
nnnnnnnnnnnnnnnn0010000111011101	LD	<r16>, <n>	d=4
nnnnnnnnnnnnnnnn0010000111111101	LD	<r16>, <n>	d=5
nnnnnnnnnnnnnnnn00101010	LD	<r16>, <nidx>	d=2
nnnnnnnnnnnnnnnn01dd101111101101	LD	<r16>, <nidx>
nnnnnnnnnnnnnnnn0010100111011101	LD	<r16>, <nidx>	d=4
nnnnnnnnnnnnnnnn0010100111111101	LD	<r16>, <nidx>	d=5
nnnnnnnnnnnnnnnn00100010	LD	<nidx>, <r16>	d=2
nnnnnnnnnnnnnnnn01dd0011	LD	<nidx>, <r16>
nnnnnnnnnnnnnnnn0010001011011101	LD	<nidx>, <r16>	d=4
nnnnnnnnnnnnnnnn0010001011111101	LD	<nidx>, <r16>	d=5
11111001	LD	<sp>, <r16>	d=2
1111100111011101	LD	<sp>, <r16>	d=4
1111100111111101	LD	<sp>, <r16>	d=5
11dd0101	PUSH	<r16>
1110010111011101	PUSH	<r16>	d=4
1110010111111101	PUSH	<r16>	d=5
11dd0001	POP	<r16>
1110000111011101	POP	<r16>	d=4
1110000111111101	POP	<r16>	d=5
11101011	EX	<de>, <hl>
00001000	EX	<af>, <afap>
11011000	EXX
11100011	EX	<spidx>, <r16>	d=2
1110001111011101	EX	<spidx>, <r16>	d=4
1110001111111101	EX	<spidx>, <r16>	d=5
1010000011101101	LDI
10110000	LDIR
1010100011101101	LDD
1011100011101101	LDDR
1010000111101101	CPI
1011000111101101	CPIR
1010100111101101	CPD
1011100111101101	CPDR
10000rrr	ADD	<A>, <r>
nnnnnnnn11000110	ADD	<A>, <n>
10000110	ADD	<A>, <hlidx>
dddddddd1000011011011101	ADD	<A>, <ixd>
dddddddd1000011011111101	ADD	<A>, <iyd>
10001rrr	ADC	<A>, <r>
nnnnnnnn11001110	ADC	<A>, <n>
10001110	ADC	<A>, <hlidx>
dddddddd1000111011011101	ADC	<A>, <ixd>
dddddddd1000111011111101	ADC	<A>, <iyd>
10010rrr	SUB	<A>, <r>
nnnnnnnn11010110	SUB	<A>, <n>
10010110	SUB	<A>, <hlidx>
dddddddd1001011011011101	SUB	<A>, <ixd>
dddddddd1001011011111101	SUB	<A>, <iyd>
10011rrr	SBC	<A>, <r>
nnnnnnnn11011110	SBC	<A>, <n>
10011110	SBC	<A>, <hlidx>
dddddddd1001011111011101	SBC	<A>, <ixd>
dddddddd1001011111111101	SBC	<A>, <iyd>
10100rrr	AND	<A>, <r>
nnnnnnnn11100110	AND	<A>, <n>
10100110	AND	<A>, <hlidx>
dddddddd1010011011011101	AND	<A>, <ixd>
dddddddd1010011011111101	AND	<A>, <iyd>
10110rrr	OR	<A>, <r>
nnnnnnnn11110110	OR	<A>, <n>
10110110	OR	<A>, <hlidx>
dddddddd1011011011011101	OR	<A>, <ixd>
dddddddd1011011011111101	OR	<A>, <iyd>
10101rrr	XOR	<A>, <r>
nnnnnnnn11101110	XOR	<A>, <n>
10101110	XOR	<A>, <hlidx>
dddddddd1010111011011101	XOR	<A>, <ixd>
dddddddd1010111011111101	XOR	<A>, <iyd>
10111rrr	CP	<A>, <r>
nnnnnnnn11111110	CP	<A>, <n>
10111110	CP	<A>, <hlidx>
dddddddd1011111011011101	CP	<A>, <ixd>
dddddddd1011111011111101	CP	<A>, <iyd>
00rrr100	INC	<r>
00110100	INC	<hlidx>
dddddddd0011010011011101	INC	<ixd>
dddddddd0011010011111101	INC	<iyd>
00rrr101	DEC	<r>
00110101	DEC	<hlidx>
dddddddd0011010111011101	DEC	<ixd>
dddddddd0011010111111101	DEC	<iyd>
00100111	DAA
00101111	CPL
0100010011101101	NEG
00111111	CCF
00110111	SCF
00000000	NOP
01110110	HALT
11110011	DI
11111011	EI
010nN11011101101	IM	<n>
00dd1001	ADD	<hl>, <r16>
01dd101011101101	ADC	<hl>, <r16>
00dd0001	SUB	<hl>, <r16>
01dd0010	SBC	<hl>, <r16>
00dd100111011101	ADD	<ix>, <r16>
00dd100111111101	ADD	<iy>, <r16>
00dd0011	INC	<r16>
0010001111011101	INC	<ix>
0010001111111101	INC	<iy>
00dd1011	DEC	<r16>
0010101111011101	DEC	<ix>
0010101111111101	DEC	<iy>
00000111	RLCA
00010111	RLA
00001111	RRCA
00011111	RRA
00000rrr11001011	RLC	<r>
0000011011001011	RLC	<hlidx>
00000110dddddddd1100101111011101	RLC	<ixd>
00000110dddddddd1100101111111101	RLC	<iyd>
00010rrr11001011	RL	<r>
0001011011001011	RL	<hlidx>
00010110dddddddd1100101111011101	RL	ixd>
00010110dddddddd1100101111111101	RL	<iyd>
00011rrr11001011	RR	<r>
0001111011001011	RR	<hlidx>
00011110dddddddd1100101111011101	RR	<ixd>
00011110dddddddd1100101111111101	RR	<iyd>
00100rrr11001011	SLA	<r>
0010011011001011	SLA	<hlidx>
00100110dddddddd1100101111011101	SLA	<ixd>
00100110dddddddd1100101111111101	SLA	<iyd>
00101rrr11001011	SRA	<r>
0010111011001011	SRA	<hlidx>
00101110dddddddd1100101111011101	SRA	<ixd>
00101110dddddddd1100101111111101	SRA	<iyd>
00110rrr11001011	SRL	<r>
0011011011001011	SRL	<hlidx>
00110110dddddddd1100101111011101	SRL	<ixd>
00110110dddddddd1100101111111101	SRL	<iyd>
0110111111101101	RLD
0110011111101101	RRD
01nnnrrr11001011	BIT	<nd>, <r>
01nnn11011001011	BIT	<nd>, <hlidx>
01nnn110dddddddd1100101111011101	BIT	<nd>, <ixd>
01nnn110dddddddd1100101111111101	BIT	<nd>, <iyd>
11nnnrrr11001011	SET	<nd>, <r>
11nnn11011001011	SET	<nd>, <hlidx>
11nnn110dddddddd1100101111011101	SET	<nd>, <ixd>
11nnn110dddddddd1100101111111101	SET	<nd>, <iyd>
10nnnrrr11001011	RES	<nd>, <r>
10nnn11011001011	RES	<nd>, <hlidx>
10nnn110dddddddd1100101111011101	RES	<nd>, <ixd>
10nnn110dddddddd1100101111111101	RES	<nd>, <iyd>
nnnnnnnnnnnnnnnn11000011	JP	<labeln>
nnnnnnnnnnnnnnnn11ccc010	JP	<c>, <labeln>
iiiiiiii00011000	JR	<labeli>
iiiiiiii001cc000	JR	<c>, <labeli>
11101001	JP	<hlidx>
1110100111011101	JP	<ixidx>
1110100111111101	JP	<iyidx>
iiiiiiii00010000	DJNZ	<labeli>
nnnnnnnnnnnnnnnn11001101	CALL	<labeln>
nnnnnnnnnnnnnnnn11ccc100	CALL	<c>, <labeln>
11001001	RET
11ccc000	RET	<c>
0100110111101101	RETI
0100010111101101	RETN
11nnn111	RST	<n8>
nnnnnnnn11011011	IN	<A>, <nidx>
01rrr00011101101	IN	<r>, <Cidx>
1010001011101101	INI
1011001011101101	INIR
1010101011101101	IND
1011101011101101	INDR
nnnnnnnn11010011	OUT	<nidx>, <A>
01rrr00111101101	OUT	<Cidx>, <r>
1010001111101101	OUTI
1011001111101101	OTIR
1010101111101101	OUTD
1011101111101101	OTDR
